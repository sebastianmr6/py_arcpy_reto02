import os
import glob
# import subprocess
from tkinter import *
from tkinter import messagebox
from tkinter.ttk import Combobox
from tkinter import filedialog

class StartGUI(Frame):
    def __init__(self, master):
        Frame.__init__(self,master)
        self.frame = Frame(master)
        
        # Botón carpeta de entrada
        self.path_in = StringVar()
        self.path_in.set('Selecciona la carpeta de entrada')
        self.lbl_path_in = Label(text=self.path_in.get())
        self.lbl_path_in.grid(sticky=W,row=0,column=0)
        self.btn_path_in = Button(text="Seleccionar", command= lambda:self.seleccinar_path_in(label=self.lbl_path_in, path_in=self.path_in)).grid(row=0,column=2)
        # Selección DEM
        self.btn_path_dem = Label(text='Selecciona un DEM').grid(sticky=W, row=1, column=0)
        self.btn_path_dem = Combobox(values=[], postcommand=self.actualizarListDEM)
        self.btn_path_dem.grid(row=1, column=2)
        # Selección Usos de suelo
        self.btn_path_land_uses = Label(text='Selecciona un Archivo Shape de ESRI de Usos de suelo', anchor=W).grid(sticky=W, row=2, column=0)
        self.btn_path_land_uses = Combobox(values=[], postcommand=self.actualizarListSHP)
        self.btn_path_land_uses.grid(row=2, column=2)
        # Selección Edafología
        self.btn_path_edafologia = Label(text='Selecciona un Archivo Shape de ESRI de Edafología').grid(sticky=W, row=3, column=0)
        self.btn_path_edafologia = Combobox(values=[], postcommand=self.actualizarListSHP)
        self.btn_path_edafologia.grid(sticky=W, row=3, column=2)
        # Selección Estaciones
        self.btn_path_estacion = Label(text='Selecciona un Archivo Shape de ESRI de Estaciones').grid(sticky=W, row=4, column=0)
        self.btn_path_estacion = Combobox(values=[], postcommand=self.actualizarListSHP)
        self.btn_path_estacion.grid(row=4, column=2)

        # Cración Carpeta de salida
        self.path_out = StringVar()
        self.path_out.set("No ha seleccionado path de salida")
        self.lbl_path_out = Label(text=self.path_out.get())
        self.lbl_path_out.grid(row=5, column=0)
        self.btn_path_out = Button(text="Seleccionar", command= lambda:self.seleccionar_path_out(self.lbl_path_out, self.path_out))
        self.btn_path_out.grid(row=5,column=2)
        self.btn_print = Button(text="Procesar", command=lambda:self.procesarModeloErosion() )
        self.btn_print.grid(row=20, column=1)
    
    def seleccinar_path_in(self, label, path_in):
        self.path=filedialog.askdirectory()
        label.config(text=self.path)
        self.path_in.set(self.path)

    def actualizarListDEM(self):
        self.lista_dem = glob.glob1(self.path_in.get(), '*.tif')
        self.btn_path_dem['values'] = self.lista_dem

    def actualizarListSHP(self):
        self.lista_shp = glob.glob1(self.path_in.get(), '*.shp')
        self.btn_path_land_uses['values'] = self.lista_shp
        self.btn_path_edafologia['values'] = self.lista_shp
        self.btn_path_estacion['values'] = self.lista_shp

    def seleccionar_path_out(self,label,path_out):
        self.path = filedialog.askdirectory()
        label.config(text= self.path)
        self.path_out.set(self.path)

    def procesarModeloErosion(self):
        self.path_input = self.path_in.get()
        self.dem = os.path.join(self.path_input, self.btn_path_dem.get())
        self.lu = os.path.join(self.path_input, self.btn_path_land_uses.get())
        self.eda = os.path.join(self.path_input, self.btn_path_edafologia.get())
        self.station = os.path.join(self.path_input, self.btn_path_estacion.get())

        if self.path_out.get() != "No ha seleccionado path de salida":
            self.path_output = self.path_out.get()
        else:
            self.path_output = ''

        if self.path_input != '' and self.btn_path_dem.get() != '' and self.btn_path_edafologia.get() != '' and self.btn_path_estacion.get() != '' and self.path_output != '':
            self.path_arcpy = os.path.join(os.getcwd(),'classes','modeloErosion.py')
            self.parametros = f' "{self.dem}" "{self.lu}" "{self.eda}" "{self.station}" "{self.path_output}"'
            self.py2_comandos = self.path_arcpy + self.parametros
            print("comandos: " + self.py2_comandos)
            self.path_interprete = os.path.join('C:', 'Python27' , 'ArcGIS10.8', 'python.exe')  # <- Acá debe cambiar la letra de la unidad y la versión de ArcGIS en caso de ser necesario
            os.system(self.path_interprete + ' ' + self.py2_comandos)
            messagebox.showinfo('Información','Se ha ejecutado el proceso')
            # process, = subprocess.Popen(self.py2_comandos.split(), stdout=subprocess.PIPE)
            # output, error = process.communicate()
            # print(error)
        else:
             messagebox.showerror('Parámetro faltante','Debe llenar todas las casillas')

