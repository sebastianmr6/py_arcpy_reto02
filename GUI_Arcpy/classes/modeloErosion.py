# -*- coding: utf-8 -*-
#!'C:\Python27\ArcGIS10.8\python.exe '  # "CAMBIAR LA LETRA DE LA UNIDAD Y LA VERSIÓN DE ARCGIS EN CASO DE SER NECESARIO"

import os
import sys

import arcpy
from arcpy.sa import Sin, Power, Con, Tan

def main(argv):
    arcpy.CheckOutExtension("Spatial")
    arcpy.env.overwriteOutput = True

    print "iniciar"
    # Input
    ASTER__DEM__tif = sys.argv[1]
    LandUse_shp = sys.argv[2]
    EDAFOLOGIA_shp = sys.argv[3]
    Stations_shp =  sys.argv[4]
    # Output
    path_out = sys.argv[5]

    print "dem " + ASTER__DEM__tif 
    print "land " + LandUse_shp 
    print "edafo " + EDAFOLOGIA_shp 
    print "station " + Stations_shp 

    # Local variables:
    Fill__2_ = os.path.join(path_out, 'fill')
    Output_drop_raster = ""
    Factor_R =  os.path.join(path_out, 'factor_r')
    Factor_K =  os.path.join(path_out, 'factor_k')
    FlowDir =   os.path.join(path_out, 'flowdir')
    FlowAcc =   os.path.join(path_out, 'flowacc')
    slope__3_ = os.path.join(path_out, 'slope')
    factor_F =  os.path.join(path_out, 'factor_f')
    factor_M =  os.path.join(path_out, 'factor_m')
    factor_L =  os.path.join(path_out, 'factor_l')
    factor_S =  os.path.join(path_out, 'factor_s')
    factor_LS = os.path.join(path_out,'Factor_LS')
    factor_LS = os.path.join(path_out, 'Factor_LS')
    Factor_C =  os.path.join(path_out, 'factor_c')
    Factor_A =  os.path.join(path_out, 'factor_a')

    # Set Geoprocessing environments
    arcpy.env.cartographicCoordinateSystem = "PROJCS['MAGNA_Colombia_Bogota',GEOGCS['GCS_MAGNA',DATUM['D_MAGNA',SPHEROID['GRS_1980',6378137.0,298.257222101]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Transverse_Mercator'],PARAMETER['False_Easting',1000000.0],PARAMETER['False_Northing',1000000.0],PARAMETER['Central_Meridian',-74.07750791666666],PARAMETER['Scale_Factor',1.0],PARAMETER['Latitude_Of_Origin',4.596200416666666],UNIT['Meter',1.0]]"
    #arcpy.env.scratchWorkspace = ""  # Poner ruta de geodata por omisión
    #arcpy.env.cartographicPartitions = ""
    #arcpy.env.referenceScale = ""
    #arcpy.env.workspace = "" # Poner ruta de geodata por omisión

    # Process: Fill
    arcpy.gp.Fill_sa(ASTER__DEM__tif, Fill__2_, "")

    # Process: Flow Direction
    arcpy.gp.FlowDirection_sa(Fill__2_, FlowDir, "NORMAL", Output_drop_raster, "D8")

    # Process: Spline
    arcpy.gp.Spline_sa(Stations_shp, "Precipitac", Factor_R, "35", "REGULARIZED", "0,1", "12")

    # Process: IDW
    arcpy.gp.Idw_sa(EDAFOLOGIA_shp, "Factor_K", Factor_K, "34", "2", "VARIABLE 12", "")

    # Process: Flow Accumulation
    arcpy.gp.FlowAccumulation_sa(FlowDir, FlowAcc, "", "FLOAT", "D8")

    # Process: Slope
    arcpy.gp.Slope_sa(Fill__2_, slope__3_, "DEGREE", "1", "PLANAR", "METER")

    raster_factor_F = (Sin(arcpy.Raster(slope__3_) *  0.01745)/ 0.0896) / (3 * Power(Sin(arcpy.Raster(slope__3_)*0.01745), 0.8)+0.56)
    raster_factor_F.save(factor_F)
    raster_factor_m = (raster_factor_F) / (1+ raster_factor_F)
    raster_factor_m.save(factor_M)
    raster_flowAcum = arcpy.Raster(FlowAcc)
    raster_factor_l = (
        (((Power((raster_flowAcum+625), (raster_flowAcum+1))) ) ) - 
        (((Power((raster_flowAcum+625), (raster_flowAcum+1))) ) ) / 
        ((Power(25, (raster_flowAcum+2))) ) * 
        (Power(22.13, raster_factor_m) )
        )
    raster_factor_l.save(factor_L)
    raster_slope = arcpy.Raster(slope__3_)
    raster_factor_s = (
        Con(
            (Tan(raster_slope * 0.01745) < 0.09),
            (10.8 * Sin(raster_slope * 0.01745) + 0.03),
            (16.8 * Sin(raster_slope * 0.01745) - 0.5))
        )
    raster_factor_s.save(factor_S)
    raster_factor_ls = (raster_factor_l * raster_factor_s)
    raster_factor_ls.save(factor_LS)

    # Process: Polygon to Raster
    arcpy.PolygonToRaster_conversion(LandUse_shp, "Factor_C", Factor_C, "CELL_CENTER", "NONE", "39")
    factor_A = os.path.join(path_out,'factor_a')
    raster_factor_a = (arcpy.Raster(Factor_R)*arcpy.Raster(Factor_K)*arcpy.Raster(factor_LS)*arcpy.Raster(Factor_C))
    raster_factor_a.save(factor_A)


if __name__ == "__main__":
    
    main(sys.argv)